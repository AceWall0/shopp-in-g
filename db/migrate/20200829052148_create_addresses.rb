class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.references :user, foreign_key: true
      t.string :alias
      t.string :receiver_name
      t.string :postal_code
      t.string :street
      t.string :number
      t.string :district
      t.string :city
      t.string :federal_unity

      t.timestamps
    end
  end
end
