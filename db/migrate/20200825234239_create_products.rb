class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.decimal :price,       index:true
      t.integer :storage,     index:true, default: 0
      t.integer :like_count,  default: 0
      t.float :weight
      t.float :width
      t.float :height
      t.float :depth
      t.integer :sold_count,  default: 0, index: true
      t.references :category, foreign_key: true
      
      t.timestamps
    end
  end
end
