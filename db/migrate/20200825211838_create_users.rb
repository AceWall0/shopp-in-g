class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_digest
      t.string :name
      t.date :birth_date
      t.string :cpf
      t.string :cnpj
      t.string :phone
      t.string :cellphone
      t.boolean :admin, default: false
      t.integer :main_address

      t.timestamps
    end
  end
end
