# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(name: 'Chapolim Colorado',
            email: 'bolhanhosCP@chavez.mex',
            password: "123456",
            birth_date: Faker::Date.birthday,
            cpf: Faker::IDNumber.brazilian_cpf,
            admin: true
)

if ENV['ADMIN_EMAIL'] && ENV['ADMIN_PASSWORD']
    User.create(name: "Admin",
                email: ENV['ADMIN_EMAIL'],
                password: ENV['ADMIN_PASSWORD'])
end

15.times do
    User.create(name: Faker::Movies::StarWars.character,
                email: Faker::Internet.email,
                password: Faker::Internet.password,
                birth_date: Faker::Date.birthday,
                cpf: Faker::IDNumber.brazilian_cpf)
end


# Creates products and categories
5.times do
    category = Faker::Commerce.department(max: 1)
    
    teste = Category.create(name: category)
    13.times do
        Product.create(name: Faker::Commerce.product_name,
                       price: Faker::Commerce.price(as_string: true),
                       category_id: teste.id,
                       description: Faker::Lorem.paragraphs.join("\n"),
                       sold_count: rand(500))
    end
end

Article.create(name: "sobre", title: "Sobre nós", content: Faker::Lorem.paragraphs.join("\n"))