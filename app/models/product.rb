class Product < ApplicationRecord
    has_one_attached :thumbnail
    has_many_attached :images
    belongs_to :category
    validates :name, :price, :category, presence: true
    validates :images, content_type: ['image/png', 'image/jpg', 'image/jpeg'], size: {less_than: 5.megabytes, message: 'Image is too large'}
    validates :thumbnail, content_type: ['image/png', 'image/jpg', 'image/jpeg'], size: {less_than: 5.megabytes, message: 'Image is too large'}
end
