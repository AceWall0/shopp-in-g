class User < ApplicationRecord
    has_secure_password
    has_many :addresses
    validates :name, :email, presence: true
    validates :email, format: {with: URI::MailTo::EMAIL_REGEXP}, :uniqueness => {:case_sensitive => false}
    validates :password, length: {minimum: 6}
end
