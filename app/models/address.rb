class Address < ApplicationRecord
  belongs_to :user
  validates :receiver_name, :postal_code, :street, :number, :district, :city, :federal_unity, presence: true
end
