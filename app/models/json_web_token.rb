class JsonWebToken 
    SECRET = ENV['SECRET_KEY']
    
    def self.encode(payload)
        JWT.encode(payload, SECRET)
    end

    def self.decode(token)
        begin
            decoded = JWT.decode(token, SECRET)
        rescue => exception
            return nil
        end
    end

end