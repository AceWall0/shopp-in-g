class Article < ApplicationRecord
    has_many_attached :images
    validates :name, uniqueness: true
    validates :name, :title, :content, presence: true
    validates :images, content_type: ['image/png', 'image/jpg', 'image/jpeg'], size: {less_than: 10.megabytes, message: 'Image is too large'}
end
