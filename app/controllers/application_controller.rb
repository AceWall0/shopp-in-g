class ApplicationController < ActionController::API
    def current_user
        token = request.headers["Authorization"]
        token = token.split(" ").last if token
        return nil unless token.present?
        @decoded = JsonWebToken.decode(token)

        return nil unless @decoded
        user = User.find_by(id: @decoded[0]["user_id"])
    end

    # get '/current_user'
    def user_must_exist
        if current_user.present?
            render json: {message: "Logged in"}
        else
            render json: {message: "Not logged in"}
        end
    end

    protected
        def must_be_logged_in
            if current_user.nil?
                render json: {message: "You are not logged in"}, status: :unauthorized
            end
        end

end
