class AdminController < ApplicationController

    def update
        @user = User.find(params[:id])
        if @user.update_attribute(:admin, user_params["admin"])
            render json: @user
        else
           render json: @user.errors, status: :unprocessable_entity
        end
    end

    def user_params
        params.require(:user).permit(:admin)
    end
end