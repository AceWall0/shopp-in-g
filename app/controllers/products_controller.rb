class ProductsController < ApplicationController
    before_action :set_product, only: [:show, :update, :destroy, :delete_image]
    load_and_authorize_resource
    
    # GET /products
    def index
        @products = Product.all
        @products = @products.where(category_id: params[:category_id]) if params[:category_id]
        @products = @products.where("name like ?", "%#{params[:search]}%") if params[:search]
        @products = @products.where("price < ?", "#{params[:below]}") if params[:below]
        @products = @products.where("price > ?", "#{params[:above]}") if params[:above]


        total_result = @products.count
        limit = params[:limit].to_i
        if limit > 0
            page = params[:page].to_i || 1
            @products = @products.limit(limit).offset(limit * (page - 1)) 
        end

        payload = []
        @products.each do |t|
            thumbnail = t.thumbnail
            thumb_json = {"url": url_for(thumbnail), "id": thumbnail.id} if thumbnail.attached?
            payload.push({
                "thumbnail": thumb_json
            }.merge(t.as_json))
        end

        render json: {
                "total_result": total_result, 
                "products": payload
            }
    end

    # GET /products/1
    def show

        # Takes the id and url of each image of the product and pushes to the array
        img_json_list = @product.images.all.map do |img| 
            {"url": url_for(img), "id": img.id}
        end

        thumbnail = @product.thumbnail
        thumbnail_json = {
            "url": url_for(thumbnail), 
            "id": thumbnail.id
        } if thumbnail.attached?

        render json: {
            "product": {
                "images": img_json_list, 
                "thumbnail": thumbnail_json
            }.merge(@product.as_json)
        }
    end

    # DELETE /products/1/images/1
    def delete_image
        @product.images.find(:img).purge
    end

    # POST /products
    def create
        @product = Product.new(product_params)

        if @product.save
        render json: @product, status: :created, location: @product
        else
        render json: @product.errors, status: :unprocessable_entity
        end
    end

    # PATCH/PUT /products/1
    def update
        if @product.update(product_params)
            render json: @product
        else
            render json: @product.errors, status: :unprocessable_entity
        end
    end

    # DELETE /products/1
    def destroy
        @product.images.all.each {|t| t.purge}
        @product.thumbnail.purge if @product.thumbnail.attached?
        @product.destroy
    end

    private
        # Use callbacks to share common setup or constraints between actions.
        def set_product
            @product = Product.find(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def product_params
            params.permit(
                :name, 
                :description, 
                :price, 
                :storage, 
                :like_count, 
                :weight, 
                :width, 
                :height, 
                :depth,
                :category, :category_id,
                :sold_count,
                :thumbnail,
                images: []
            )
        end
end
