class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :update, :destroy]
  before_action :must_be_logged_in
  load_and_authorize_resource

   # GET /users/1/adresses
  def show_addresses
    @user = User.find(params[:id])      # COMO CARAIO
    authorize! :show_addresses, @user   # FAZ O USUARIO VER APENAS
    render json: @user.addresses        # OS PRÓPRIOS ENDEREÇOS?
  end


  # GET /addresses
  def index
    @addresses = Address.all

    render json: @addresses
  end

  # GET /addresses/1
  def show
    render json: @address
  end

  # POST /addresses
  def create
    @address = Address.new(address_params)

    if @address.save
      render json: @address, status: :created, location: @address
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /addresses/1
  def update
    if @address.update(address_params)
      render json: @address
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # DELETE /addresses/1
  def destroy
    @address.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = Address.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def address_params
      params.require(:address).permit(:user_id, :alias, :receiver_name, :postal_code, :street, :number, :district, :city, :federal_unity)
    end
end
