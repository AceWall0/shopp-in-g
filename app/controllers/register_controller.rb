class RegisterController < ApplicationController
    # POST /signup
    def create
        @user = User.new(user_params)

        if @user.save
        render json: @user, status: :created, location: @user
        else
        render json: @user.errors, status: :unprocessable_entity
        end
    end

    private
        # Only allow a trusted parameter "white list" through.
        def user_params
        params.require(:user).permit(
            :email, 
            :password, 
            :password_confirmation, 
            :name, 
            :birth_date, 
            :cpf, 
            :cnpj, 
            :phone, 
            :cellphone, 
            :admin, 
            :main_address
        )
        end
end