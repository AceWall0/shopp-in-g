class StatisticsController < ApplicationController
    def sold_sum_by_category
        @payload = []
        
        @categories = Category.all
        @categories.each do |category|
            @payload.push(
                {
                    "category_id": category.id,
                    "category_name": category.name,
                    "sold_sum": category.products.sum(:sold_count)
                }
            )
        end

        render json: @payload
    end
end