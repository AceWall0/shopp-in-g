# README

-----
## Produtos:
- Listar produtos:  
GET: `/products`

- Criar produtos:  
POST: `/products`  
parâmetros:  
-- name: string  
-- price: decimal  
-- category_id: integer  
-- images[]: type/image[]  
-- thumbnail: type/image  
-- weight: float  
-- width: float  
-- height: float  
-- depth: float





-----
## For developers
How to reset heroku's db:

1. `heroku pg:reset DATABASE_URL`
1. `heroku rake db:schema:load`
1. `heroku rake db:migrate`
1. `heroku rake db:seed`