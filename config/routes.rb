Rails.application.routes.draw do
  resources :addresses
  get '/users/:id/addresses', to: 'addresses#show_addresses'
  resources :articles
  resources :categories
  resources :users, except: [:create, :set_admin], path: '/dashboard/users'
  resources :products
  delete '/products/:id/images/:img', to: 'products#delete_image'
  delete '/images/:img', to: 'products#delete_image'
  
  put   '/dashboard/users/:id/set_admin', to: 'admin#update'
  patch '/dashboard/users/:id/set_admin', to: 'admin#update'
  post '/signup', to: 'register#create'
  post '/login', to: 'session#login'

  get '/statistics/sold_sum_by_category', to: 'statistics#sold_sum_by_category' 
  
  # get '/current_user', to: 'application#user_must_exist'

  get '/', to: 'static_page#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
